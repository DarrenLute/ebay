export interface EarthquakeContract {
  datetime: Date;
  depth: number;
  lng: number;
  src: string;
  eqid: string;
  magnitude: number;
  lat: number;
}
