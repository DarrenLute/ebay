import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { EarthquakeContract } from '../contract/earthquake.contract';
import { EarthquakesStoreService } from '../stores/earthquakes-store.service';
import { DisplayEarthquakeDialogComponent } from '../dialog/display-earthquake/display-earthquake-dialog.component';


@Component({
  selector: 'app-earthquake',
  templateUrl: './earthquake.component.html',
  styleUrls: ['./earthquake.component.scss']
})
export class EarthquakeComponent {

  constructor(
    public earthquakes: EarthquakesStoreService,
    public dialog: MatDialog
  ) { }

  openDialog(earthquakeData: EarthquakeContract): void {
    const dialogRef = this.dialog.open(DisplayEarthquakeDialogComponent, {
      data: earthquakeData
    });
  }

}
