import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';

import { EarthquakesStoreService } from './earthquakes-store.service';


describe('EarthquakesService', () => {
  let service: EarthquakesStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HttpClient,
        HttpHandler
      ]
    });
    service = TestBed.inject(EarthquakesStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
