import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BehaviorSubject } from 'rxjs';

import { EarthquakeContract } from '../contract/earthquake.contract';


@Injectable({
  providedIn: 'root'
})
export class EarthquakesStoreService {
  private earthQuakeUrl = 'http://api.geonames.org/earthquakesJSON?formatted=true&north=44.1&south=-9.9&east=-22.4&west=55.2&username=mkoppelman';

  private earthquakeListSource: BehaviorSubject<Array<EarthquakeContract>> = new BehaviorSubject([]);
  public readonly earthquakeList = this.earthquakeListSource.asObservable();

  constructor(
    private httpClient: HttpClient
  ) {
    this.getEarthquakes().subscribe(
      earthquakeData => {
        this.earthquakeListSource.next(earthquakeData.earthquakes);
      }, error => {
        console.log('Error occurred while trying to retrieve data: ', error);
      }
    );
  }

  getEarthquakes(): any {
    return this.httpClient.get(this.earthQuakeUrl);
  }
}
