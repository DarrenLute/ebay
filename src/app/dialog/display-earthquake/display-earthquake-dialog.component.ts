import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { EarthquakeContract } from '../../contract/earthquake.contract';

@Component({
  selector: 'app-display-earthquake',
  templateUrl: './display-earthquake-dialog.component.html',
  styleUrls: ['./display-earthquake-dialog.component.scss']
})
export class DisplayEarthquakeDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DisplayEarthquakeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: EarthquakeContract
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
